/*** includes ***/

#define _DEFAULT_SOURCE
#define _BSD_SOURCE
#define _GNU_SOURCE

#include <ctype.h>
#include <errno.h>
#include <fcntl.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <termios.h>
#include <time.h>
#include <unistd.h>

/*** macros ***/

#define CTRL_PLUS(key) (key & 0x1f)
#define KILO_VERSION "0.0.1"
#define TAB_STOP 8
#define KILO_QUIT_TIMES 3

enum editor_keys {
	BACKSPACE = 127,
	ARROW_UP = 1000,
	ARROW_DOWN,
	ARROW_LEFT,
	ARROW_RIGHT,
	HOME_KEY,
	END_KEY,
	DEL_KEY,
	PAGE_UP,
	PAGE_DOWN,
};

enum editor_highlight {
	HL_NORMAL = 0,
	HL_COMMENT,
	HL_MLCOMMENT,
	HL_KEYWORD1,
	HL_KEYWORD2,
	HL_NUMBER,
	HL_MATCH,
	HL_STRING
};

void editor_set_status_message(const char *fmt, ...);
void editor_refresh_screen();
char *editor_prompt(char *prompt, void (*callback)(char*, int));

#define HL_HIGHLIGHT_STRING 1 << 0
#define HL_HIGHLIGHT_NUMBERS 1 << 1

/*** data ***/

struct editor_syntax {
	char *filetype;
	const char **filematch;
	const char **keywords;
	// Each language will have its own comment tag eg.. //, /*, */ for C/C++
	char single_line_comment[2];
	char *multiline_comment_start;
	char *multiline_comment_end;
	int flags;
};

typedef struct editor_row {
	int idx;
	int size;
	int r_size;
	char* chars;
	char* render; // Transforming certain chars in the files to render on screen
	unsigned char* hl; // Array storing the class of chars in render for syntax H/L
	int hl_open_comment; // Has open multiline comment
} editor_row;

struct editor_config
{
	struct termios term_flags;
	int window_rows;
	int window_cols;
	int num_rows;
	int row_offset; // The first row that will be visible to the user..Used for vertical scrolling
	int col_offset; // For enabling horizontal scrolling
	editor_row *e_row; // Will be used as a pointer to a dynamic array
	int cx, cy; // Cursor position within the file. Zero-based index, bcoz C uses zero based indices
	int rx; // Horizontal cursor position within the render array
	int dirty; // To keep track of modifications made to file
	char *filename;
	char statusmsg[80];
	time_t statusmsg_time;
	struct editor_syntax *syntax;
};
struct editor_config editor;

/*** Syntax Highlight DB ***/

const char *C_HL_extensions[] = {".c", ".cc", ".cpp", ".h", ".hpp", NULL};
const char *C_HL_keywords[] = {
	/* C Keywords */
	"auto","break","case","continue","default","do","else","enum",
	"extern","for","goto","if","register","return","sizeof","static",
	"struct","switch","typedef","union","volatile","while","NULL",

	/* C++ Keywords */
	"alignas","alignof","and","and_eq","asm","bitand","bitor","class",
	"compl","constexpr","const_cast","deltype","delete","dynamic_cast",
	"explicit","export","false","friend","inline","mutable","namespace",
	"new","noexcept","not","not_eq","nullptr","operator","or","or_eq",
	"private","protected","public","reinterpret_cast","static_assert",
	"static_cast","template","this","thread_local","throw","true","try",
	"typeid","typename","virtual","xor","xor_eq",

	/* Preprocessor Directives */
	"#include", "#define",
	/* C types */
	"int|","long|","double|","float|","char|","unsigned|","signed|",
	"void|","short|","auto|","const|","bool|", NULL
};
struct editor_syntax HLDB[] = {
	{
		"c",
		C_HL_extensions,
		C_HL_keywords,
		"//",
		"/*",
		"*/",
		HL_HIGHLIGHT_NUMBERS | HL_HIGHLIGHT_STRING
	}
};

#define HLDB_ENTRIES sizeof(HLDB) / sizeof(HLDB[0])

/*** terminal ***/

void pkill(const char *s) {
	write(STDOUT_FILENO, "\x1b[2J", 4);
	write(STDOUT_FILENO, "\x1b[H", 3);
	perror(s);
	exit(1);
}

void disable_raw_mode() {
	if(tcsetattr(STDIN_FILENO, TCSAFLUSH, &editor.term_flags) == -1) pkill("tcsetattr");
}

void enable_raw_mode() {
	if(tcgetattr(STDIN_FILENO, &editor.term_flags) == -1) pkill("tcgetattr");
	struct termios raw = editor.term_flags;
	raw.c_iflag &= ~(IXON | ICRNL | INPCK | ISTRIP | BRKINT) ;
	raw.c_oflag &= ~(OPOST);
	raw.c_lflag &= ~(ECHO | ICANON | ISIG | IEXTEN);
	raw.c_cflag |= CS8;
	raw.c_cc[VMIN] = 0;
	raw.c_cc[VTIME] = 1;
	if(tcsetattr(STDIN_FILENO, TCSAFLUSH, &raw) == -1) pkill("tcsetattr");
}

int editor_read_keypress() {
	int num_read;
	char c;
	while ((num_read = read(STDIN_FILENO, &c, 1)) != 1) {
		if (num_read == -1 && errno != EAGAIN) pkill("read");
	}
	// Check for a escape sequence
	if (c == '\x1b') {
		char seq[3]; // All sequences have 2 or 3 bytes after ESC.
		// If the next two reads time out we assume the user just pressed ESC
 		if (read(STDIN_FILENO, &seq[0], 1) != 1) return '\x1b'; 
		if (read(STDIN_FILENO, &seq[1], 1) != 1) return '\x1b';
		if (seq[0] == '[') {
			if (seq[1] >= '0' && seq[1] <= '9') {
				if (read(STDIN_FILENO, &seq[2], 1) != 1) return '\x1b';
				if (seq[2] == '~') {
					switch (seq[1]) {
					case '1' : return HOME_KEY;
					case '3' : return DEL_KEY;
					case '4' : return END_KEY;
					case '5': return PAGE_UP;
					case '6': return PAGE_DOWN;
					case '7' : return HOME_KEY;
					case '8' : return END_KEY;
					}
				}
			} else {
				switch (seq[1])
				{
				case 'A': return ARROW_UP;
				case 'B': return ARROW_DOWN;
				case 'C': return ARROW_RIGHT;
				case 'D': return ARROW_LEFT;
				case 'H' : return HOME_KEY;
				case 'F' : return END_KEY;
				}
			}
		} else if (seq[0] == 'O') {
			switch (seq[1])
				{
				case 'H' : return HOME_KEY;
				case 'F' : return END_KEY;
				}
		}
		return '\x1b'; // Return ESC if escape sequence not recognised
	} else {
		return c;
	}
}

int get_cursor_position(int *rows, int *cols) {
	char buf[32];
  	unsigned int i = 0;
	if (write(STDOUT_FILENO, "\x1b[6n", 4) != 4) return -1;
	while (i < sizeof(buf) - 1) {
		if (read(STDIN_FILENO, &buf[i], 1) != 1) break;
		if (buf[i] == 'R') break;
		i++;
	}
  buf[i] = '\0';
  if (sscanf(buf, "\x1b[%d;%d", rows, cols) != 2) return -1; // Different from Orig
  return 0;
}

int get_window_size(int *rows, int *cols) {
	struct winsize terminal_size;
	if (ioctl(STDOUT_FILENO, TIOCGWINSZ, &terminal_size) == -1 || terminal_size.ws_col == 0) {
		if (write(STDOUT_FILENO, "\x1b[999C\x1b[999B", 12) != 12) return -1;
		else return get_cursor_position(rows, cols);
	} else {
		*rows = terminal_size.ws_row;
		*cols = terminal_size.ws_col;
		return 0;
	}
}

/*** syntax highlighting ***/

int is_seperator(int c) {
	return isspace(c) || c == '\0' || strchr(",.()+-/*=~%<>[];", c) != NULL;
}

void editor_update_syntax(editor_row *row) {
	row->hl = realloc(row->hl, row->r_size);
	memset(row->hl, HL_NORMAL, row->r_size);

	if (!editor.syntax) return;

	char *scs = editor.syntax->single_line_comment;
	char *mcs = editor.syntax->multiline_comment_start;
	char *mce = editor.syntax->multiline_comment_end;
	int scs_len = scs ? strlen(scs) : 0; 
	int mcs_len = mcs ? strlen(mcs) : 0;
	int mce_len = mce ? strlen(mce) : 0;
	const char **keywords = editor.syntax->keywords;
	
	int prev_sep = 1; // Flag..If previous char is a seperator it's 1 else 0. 1 initially
	// Flag for tracking if row is in multiline comments
	int in_comment = (row->idx > 0 && editor.e_row[row->idx-1].hl_open_comment);

	int i = 0;
	while (i < row->r_size) {
		char c = row->render[i];
		unsigned char prev_hl = i > 0 ? row->hl[i-1] : HL_NORMAL;
		
		// Highlight single line comments if defined
		if (scs_len && !in_comment) { // Different from Orig
			if (!strncmp(row->render+i, scs, scs_len)) {
				memset(row->hl+i, HL_COMMENT, row->r_size - i);
				return;
			}
		}

		// Highlight multiline line comments if defined
		if (mce_len && mcs_len){
			if (in_comment) {
				if (!strncmp(row->render+i, mce, mce_len)) {
					memset(row->hl+i, HL_COMMENT, mce_len);
					i += mce_len;
					in_comment = 0;
					prev_sep = 1;
					continue;
				}
				row->hl[i] = HL_COMMENT;
				i++;
				continue;
			} else if (!strncmp(row->render+i, mcs, mcs_len)) {
			memset(row->hl+i, HL_COMMENT, mcs_len);
			i += mcs_len;
			in_comment = 1;
			continue;
			}
		}

		if (editor.syntax->flags & HL_HIGHLIGHT_STRING) { // Different from Orig
			if (c == '"' || c == '\'') {
				do {
					row->hl[i] = HL_STRING;
					if (row->render[i] == '\\' && i+1< row->r_size) { // Consume escape character
						row->hl[i+1] = HL_STRING;
						i++;
					}
				} while(i+1 < row->r_size && row->render[++i] != c);
				row->hl[i] = HL_STRING;
				i++;
				prev_sep = 1;
				continue;
			}
		}

		// bitwise and b/w editors flags and flag to find if it is set in flags
		if(editor.syntax->flags & HL_HIGHLIGHT_NUMBERS) {	
			if ((isdigit(c) && (prev_sep || prev_hl == HL_NUMBER)) || 
				(c ==  '.' && prev_hl == HL_NUMBER)) {
				row->hl[i] = HL_NUMBER;
				prev_sep = 0;
				i++;
				continue;
			}
		}
		
		// Keywords H/L.. Have to be preceded and succeeded by a seperator
		if(prev_sep) {
			int j = 0;
			while (keywords[j]) {
				const char* keyword = keywords[j];
				int k_len = strlen(keyword);
				int kw2 = keyword[k_len-1] == '|';
				if (kw2) k_len--;
				if (!strncmp(row->render+i, keyword, k_len) && 
					is_seperator(*(row->render+i+k_len))) {
					memset(row->hl+i, kw2 ? HL_KEYWORD2 : HL_KEYWORD1, k_len);
					i += k_len;
					break;
				}
				j++;
			}
			if (keywords[j] != NULL) {
				prev_sep = 0;
				continue;
			}
		}

		prev_sep = is_seperator(c);
		i++;
	}
	int changed = (row->hl_open_comment != in_comment);
	row->hl_open_comment = in_comment;
	if (changed && row->idx + 1 < editor.num_rows ) {
		editor_update_syntax(editor.e_row + row->idx + 1);
	}
}

int editor_syntax_to_color(int hl) {
	switch (hl)
	{
	case HL_COMMENT:
	case HL_MLCOMMENT:
		return 36;
	case HL_KEYWORD1:
		return 33;
	case HL_KEYWORD2:
		return 32;
	case HL_NUMBER:
		return 31;
	case HL_MATCH:
		return 34;
	case HL_STRING:
		return 35;
	default:
		return 37;
	}
}

void editor_select_syntax_highlight() {
	editor.syntax = NULL;
	if (!editor.filename) return;
	unsigned long i;
	for (i=0; i < HLDB_ENTRIES; i++) {
		struct editor_syntax *s = HLDB + i;
		unsigned int j=0;
		while(s->filematch[j]) {
			char *p;
			int patlen = strlen(s->filematch[j]);
			if ((p = strstr(editor.filename, s->filematch[j])) != NULL) {
				// filematch need not be an extension. Eg. Makefile
				if (s->filematch[j][0] != '.' || p[patlen] == '\0') {
					editor.syntax = s;
					int filerow = 0;
					for (filerow = 0; filerow < editor.num_rows; filerow++)
						editor_update_syntax(editor.e_row+filerow);
					return;
				}
			}
			j++;
		}
	}
}

/*** row operations ***/

int editor_convert_cx_to_rx (editor_row *row, int cx) {
	int rx = 0;
	for (int i = 0; i < cx; i++) {
		if (row->chars[i] == '\t')
			rx += (TAB_STOP - 1) - (rx % TAB_STOP);
		rx++;
	}
	return rx;
}

void editor_update_row(editor_row *row) {
	int index = 0, num_tabs = 0;
	for (int i=0; i < row->size; i++) {
		if (row->chars[i] == '\t') num_tabs++;
	}
	free(row->render);
	row->render = malloc(row->size + num_tabs*(TAB_STOP - 1) + 1);
	for (int i=0; i < row->size; i++) {
		if (row->chars[i] == '\t') {
			row->render[index++] = ' ';
			while (index % TAB_STOP != 0) row->render[index++] = ' ';
		} else {
			row->render[index++] = row->chars[i];
		}
	}
	row->render[index] = '\0';
	row->r_size = index;
	editor_update_syntax(row);
}

void editor_insert_row(int at, char* s, size_t len) {
	if (at < 0 || at > editor.num_rows) return;
	editor.e_row = realloc(editor.e_row, sizeof(editor_row) * (editor.num_rows + 1));
	memmove(editor.e_row+at+1, editor.e_row+at, sizeof(editor_row) * (editor.num_rows-at));
	for (int i = at+1; i <= editor.num_rows; i++) editor.e_row[i].idx++;

	editor.e_row[at].idx = at;
	editor.e_row[at].size = len; // Exculding null character terminator
	editor.e_row[at].chars = malloc(len + 1);
	memcpy(editor.e_row[at].chars, s, len);
	editor.e_row[at].chars[len] = '\0';
	editor.e_row[at].r_size = 0;
	editor.e_row[at].render = NULL;
	editor.e_row[at].hl = NULL;
	editor.e_row[at].hl_open_comment = 0;
	editor_update_row(&editor.e_row[at]);
	editor.num_rows++;
	editor.dirty++;
}

void editor_free_row(editor_row* row) {
	free(row->chars);
	free(row->render);
	free(row->hl);
}

void editor_del_row(int at) {
	editor_row* row;
	
	if (at < 0 || at >= editor.num_rows) return;
	row = editor.e_row + at;
	editor_free_row(row);
	memmove(editor.e_row+at, editor.e_row+at+1, sizeof(editor_row)*(editor.num_rows-at-1));
	for (int i = at; i < editor.num_rows-1; i++) editor.e_row[i].idx--;
	editor.num_rows--;
	editor.dirty++;
}

void editor_row_insert_char(editor_row *row, int at, int c) {
	if (at < 0 || at > row->size) at = row->size;
	// row->size doesn't include null so old size is actually row->size + 1
	row->chars = realloc(row->chars, row->size+2);
	memmove(&row->chars[at+1], &row->chars[at], row->size-at+1);
	row->chars[at] = c;
	row->size++;
	editor_update_row(row);
	editor.dirty++;
}

void editor_row_del_char(editor_row *row, int at) {
	if (at < 0 || at > editor.cx) return;
	memmove(&row->chars[at], &row->chars[at+1], row->size-at);
	row->size--;
	editor_update_row(row);
	editor.dirty++;
}

void editor_row_append_string(editor_row *row, char* s, int len) {
	row->chars = realloc(row->chars, row->size+len+1);
	memcpy(row->chars + row->size, s, len);
	row->size += len;
	row->chars[row->size] = '\0';
	editor_update_row(row);
	editor.dirty++;
}

/*** editor operations ***/

void editor_insert_char(int c) {
	if (editor.cy == editor.num_rows) { // If empty row, create it before inserting chars
		editor_insert_row(editor.num_rows, "", 0);
	}
	editor_row_insert_char(&editor.e_row[editor.cy], editor.cx, c);
	editor.cx++;
}

void editor_insert_new_line() {
	if (editor.cx == 0) {
		editor_insert_row(editor.cy, "", 0);
	} else {
		editor_row *row = &editor.e_row[editor.cy];
		editor_insert_row(editor.cy+1, &row->chars[editor.cx], row->size-editor.cx);
		row = &editor.e_row[editor.cy]; // Because realloc in editor_insert_row may change the location
		row->size = editor.cx;
		row->chars[row->size] = '\0';
		editor_update_row(row);
	}
	editor.cy++;
	editor.cx = 0;
}

void editor_del_char() {
	if (editor.cy == editor.num_rows) return;
	if (editor.cx == 0 && editor.cy == 0) return;
	editor_row *row = &editor.e_row[editor.cy];
	if (editor.cx > 0) {
	editor_row_del_char(row, editor.cx-1);
	editor.cx--;
	} else {
		editor.cx = editor.e_row[editor.cy-1].size;
		editor_row_append_string(&editor.e_row[editor.cy-1], row->chars, row->size);
		editor_del_row(editor.cy);
		editor.cy--;
	}
}

/*** file i/o ***/

char *editor_row_to_string(int *buf_len) {
	int row, buf_index, total_len = 0;
	char *buf;
	for (row = 0; row < editor.num_rows; row++) {
		total_len += editor.e_row[row].size + 1; // 1 added to accomodate \n not present in row.chars
	}
	*buf_len = total_len;
	buf = malloc(total_len);
	for (row = 0, buf_index=0; row < editor.num_rows; row++, buf_index++) {
		memcpy(&buf[buf_index], editor.e_row[row].chars, editor.e_row[row].size);
		buf_index += editor.e_row[row].size;
		buf[buf_index] = '\n';
	}
	return buf;
}

void editor_open(char* filename) {
	free(editor.filename);
	size_t fn_len = strlen(filename)+1; // +1 for null
	editor.filename = malloc(fn_len);
	memcpy(editor.filename, filename, fn_len);
	char *line = NULL;
	size_t line_cap = 0;
	ssize_t line_len = 0;
	FILE *fp  = fopen(filename, "r");
	if (!fp) pkill("fopen");
	editor_select_syntax_highlight();
	while ((line_len = getline(&line, &line_cap, fp)) != -1) {
		// Remove \n and \r at the end of the line read. We add that manually.
		// line_len includes \n as well. Read getline for more info
		while (line_len > 0 && (line[line_len-1] == '\r' || line[line_len-1] == '\n'))
		line_len--;
		editor_insert_row(editor.num_rows, line, line_len);
	}
	free(line);
	fclose(fp);
	editor.dirty = 0;
}

int editor_save() {
	if (editor.filename == NULL) {
		editor.filename = editor_prompt("Save File as: %s (ESC to cancel)", NULL);
		if (!editor.filename) {
			editor_set_status_message("Save aborted");
			return 1;
		}
		editor_select_syntax_highlight();
	}

	while (1) {
		editor_set_status_message("Save File?: Y-Yes N-No");
		editor_refresh_screen();
		char c = editor_read_keypress();
		if (c == 'y' || c == 'Y') {
			break;
		} else if (c == 'n' || c == 'N') {
			editor_set_status_message("Save aborted");
			return 1;
		}
	}

	int len;
	char* buf = editor_row_to_string(&len);
	int fd = open(editor.filename, O_RDWR | O_CREAT, 0644);
	if (fd == -1) goto save_err;
	if (ftruncate(fd, len) == -1) goto save_err;
	if (write(fd, buf, len) != len) goto save_err;
	close(fd);
	free(buf);
	editor_set_status_message("%d bytes written to disk", len);
	editor.dirty = 0;
	return 0;

	save_err:
	free(buf);
	if (fd != -1) close(fd);
	editor_set_status_message("Can't Save I/O Error: %s", strerror(errno));
	return 1;
}

/*** find ***/
void editor_find_callback(char* query, int key) { //Different from Orig
	static int last_match = -1;
	static int direction = 1;
	static int saved_hl_line = -1;
	static char* saved_hl = NULL;
	if (saved_hl)
	{
		memcpy(editor.e_row[saved_hl_line].hl, saved_hl, editor.e_row[saved_hl_line].r_size);
		free(saved_hl);
		saved_hl = NULL;
	}
	if (key == '\r' || key == '\x1b') {
		last_match = -1;
		direction = 1;
		return;
	} else if (key == ARROW_RIGHT || key == ARROW_DOWN) {
		direction = 1;
	} else if (key == ARROW_LEFT || key == ARROW_UP) {
		direction = -1;
	} else {
		last_match = -1;
		direction = 1;
	}
	if (last_match == -1) direction = 1;
	int i, current = last_match;
	for (i=0; i < editor.num_rows; i++) {
		current += direction;
		if (current == -1) current = editor.num_rows-1;
		else if (current == editor.num_rows) current = 0;
		char* row = editor.e_row[current].chars;
		char *match = strstr(row, query);
		if (match) {
			last_match = current;
			editor.cx = match - row;
			editor.cy = current;
			editor.row_offset = editor.cy;
			saved_hl_line = current;
			editor_row *row = &editor.e_row[editor.cy];
			saved_hl = malloc(row->r_size);
			memcpy(saved_hl, row->hl, row->r_size);
			int rx = editor_convert_cx_to_rx(row, editor.cx);
			memset(row->hl+rx, HL_MATCH, strlen(query));
			break;
		} 
	}
}

void editor_find(){
	int saved_cx = editor.cx;
	int saved_cy = editor.cy;
	int saved_row_off = editor.row_offset;
	int saved_col_off = editor.col_offset;
	char *query = editor_prompt("Search: %s (Use ESC/Arrows/Enter)", editor_find_callback);
	
	if (query) free(query);
	else {
		editor.cx = saved_cx;
		editor.cy = saved_cy;
		editor.row_offset = saved_row_off;
		editor.col_offset = saved_col_off;
	}
}

/*** seek ***/

void editor_seek() {
	if (editor.num_rows == 0) {
		editor_set_status_message("Empty file");
		return;
	}
	char* line = editor_prompt("Enter line number: %s (ESC to Exit, ENT to seek)", NULL);
	if (line) {
		char* end;
		long int line_number;
		
		errno = 0;
		line_number = strtol(line, &end, 10);
		if (errno || *end || line_number <= 0) {
			editor_set_status_message("Invalid line number");
		} else {
			if (line_number > editor.num_rows) line_number = editor.num_rows+1; // To the empty line in the end
			editor.cy = line_number-1;
		}
		free(line);
	}
}

/*** append buffer ***/

struct abuf {
	char *buf;
	int len;
	int size;
};

#define ABUF_INIT {NULL, 0, 0}

void buf_append(struct abuf *ab, char *s, int len) { // Different from Orig
	if (ab->len + len > ab->size) {
		char *new = realloc(ab->buf, (ab->len+len)*2);
		if (new == NULL) {
			return;
		}
		ab->buf = new;
		ab->size = (ab->len + len) * 2;
	}
	memcpy(&(ab->buf[ab->len]), s, len);
	ab->len += len;
}

void buf_free(struct abuf *ab) {
	free(ab->buf);
}

/*** input ***/

char *editor_prompt(char *prompt, void (*callback)(char*, int)) {
	char* buf = NULL;
	size_t buf_size = 128;
	size_t buf_len = 0;

	buf = malloc(buf_size);
	buf[0] = '\0';
	while(1) {
		int c;
		editor_set_status_message(prompt, buf);
		editor_refresh_screen();
		c = editor_read_keypress();
		if (c == '\r') {
			if(buf_len){
				editor_set_status_message("");
				if (callback) callback(buf, c);
				return buf;
			}
		} else if (!iscntrl(c) && c < 128) {
			if (buf_len == buf_size-1) {
				buf_size *= 2;
				buf = realloc(buf, buf_size);
			}
			buf[buf_len++] = c;
			buf[buf_len] = '\0';
		} else if (c == '\x1b') {
			editor_set_status_message("");
			if (callback) callback(buf, c);
			free(buf);
			return NULL;
		} else if (c == BACKSPACE || c == CTRL_PLUS('H') || c == DEL_KEY) {
			if (buf_len) {
				buf[--buf_len] = '\0';
			}
		}
		if (callback) callback(buf, c);
	}
}

void editor_move_cursor(int key) {
	editor_row *current_row = editor.cy >= editor.num_rows ? NULL : &editor.e_row[editor.cy];
	switch (key)
	{
	case ARROW_UP:
		if (editor.cy != 0) editor.cy--;
		break;
	case ARROW_LEFT:
		if (editor.cx != 0) editor.cx--;
		else if (editor.cy > 0) {
			editor.cy--;
			editor.cx = editor.e_row[editor.cy].size;
		}
		break;
	case ARROW_DOWN:
		if (editor.cy < editor.num_rows) editor.cy++;
		break;
	case ARROW_RIGHT:
		if (current_row && editor.cx < current_row->size) editor.cx++;
		else if (current_row && editor.cx == current_row->size) {
			editor.cy++;
			editor.cx = 0;
		}
		break;
	}
	current_row = editor.cy >= editor.num_rows ? NULL : &editor.e_row[editor.cy];
	int row_len = current_row ? current_row->size : 0;
	if (editor.cx > row_len) editor.cx = row_len;
}

void editor_process_keypress() {
	static int quit_times = KILO_QUIT_TIMES;
	int c = editor_read_keypress();
	switch (c) {
	case '\r':
		editor_insert_new_line();
		break;
	case CTRL_PLUS('f'):
		editor_find();
		break;
	case CTRL_PLUS('g'):
		editor_seek();
		break;
	case CTRL_PLUS('q'):
		if (editor.dirty && quit_times) {
			editor_set_status_message("WARNING! File has unsaved changes. "
			"Press CTRL-Q %d more times to quit", quit_times--);
			return;
		}
		write(STDOUT_FILENO, "\x1b[2J", 4);
		write(STDOUT_FILENO, "\x1b[H", 3);
		exit(0);
		break;
	case CTRL_PLUS('s'):
		editor_save();
		break;
	case HOME_KEY:
		editor.cx = 0;
		break;
	case END_KEY:
		if (editor.cy < editor.num_rows) {
			editor.cx = editor.e_row[editor.cy].size;
		}
		break;
	case BACKSPACE:
	case CTRL_PLUS('h'):
	case DEL_KEY:
		// Moving cursor to right and backspace is equivalent to DELETE
		if (c == DEL_KEY) editor_move_cursor(ARROW_RIGHT); 		
		editor_del_char();
		break;
	case PAGE_UP:
	case PAGE_DOWN:
		if (c == PAGE_UP) {
			editor.cy = editor.row_offset;
		} else if(c == PAGE_DOWN) {
			editor.cy = editor.row_offset + editor.window_rows - 1;
			if (editor.cy > editor.num_rows) editor.cy = editor.num_rows;
		}
		for (int i = editor.window_rows; i > 0 ; i--) {
			editor_move_cursor(c == PAGE_UP ? ARROW_UP : ARROW_DOWN);
		}
		break;
	case ARROW_UP:
	case ARROW_DOWN:
	case ARROW_LEFT:
	case ARROW_RIGHT:
		editor_move_cursor(c);
		break;
	case CTRL_PLUS('l'):
	case '\x1b':
		break;
	default:
		editor_insert_char(c);
		break;
	}
	quit_times = KILO_QUIT_TIMES;
}

/*** output ***/

void editor_scroll() {
	editor.rx = 0;
	if (editor.cy < editor.num_rows) {
		editor.rx = editor_convert_cx_to_rx(&editor.e_row[editor.cy], editor.cx);
	}

	if (editor.cy < editor.row_offset) {
		editor.row_offset = editor.cy;
	}
	if (editor.cy >= editor.row_offset + editor.window_rows) {
		editor.row_offset = editor.cy - editor.window_rows + 1;
	}
	if (editor.rx < editor.col_offset) {
		editor.col_offset = editor.rx;
	}
	if (editor.rx >= editor.col_offset + editor.window_cols) {
		editor.col_offset = editor.rx - editor.window_cols + 1;
	}
}

void editor_draw_rows(struct abuf *ab){
	int row;
	for (row = editor.row_offset; row < editor.row_offset + editor.window_rows; row++) { // Different from original
		if (row >= editor.num_rows){
			if (editor.num_rows == 0 && row == editor.window_rows / 2) {
				char welcome[100];
				int welcome_len, padding;
				welcome_len = snprintf(welcome, sizeof welcome, 
				"Kilo - A truly free editor -- version %s", KILO_VERSION);
				if (welcome_len >= editor.window_cols) welcome_len = editor.window_cols;
				padding = (editor.window_cols - welcome_len) / 2;
				if (padding) {
					buf_append(ab, "~", 1);
					padding--;
				}
				while (padding--) buf_append(ab, " ", 1);
				buf_append(ab, welcome, welcome_len);
			} else {
				buf_append(ab, "~", 1);
			}
		} else {
			int len = editor.e_row[row].r_size - editor.col_offset;
			if (len < 0) len = 0;
			if (len > editor.window_cols) len = editor.window_cols;
			char *c = &editor.e_row[row].render[editor.col_offset];
			unsigned char *hl = &editor.e_row[row].hl[editor.col_offset];
			int i;
			int current_color = -1; // -1 for default color
			for(i=0; i < len; i++) {
				// Convert control character to printable character
				if (iscntrl(c[i])) { // Different from Orig
					char sym = c[i] <= 26 ? '@' + c[i] : '?';
					buf_append(ab, "\x1b[7m", 4);
					buf_append(ab, &sym, 1);
					buf_append(ab, "\x1b[m", 3);
				} else if (hl[i] == HL_NORMAL) {
					if (current_color != -1) {
						buf_append(ab, "\x1b[39m", 5);
						current_color = -1;
					}
					buf_append(ab, c+i, 1);
				} else {
					int color = editor_syntax_to_color(hl[i]);
					if (color != current_color) {
						char buf[16];
						int clen = snprintf(buf, sizeof(buf), "\x1b[%dm", color);
						buf_append(ab, buf, clen);
						current_color = color;
					}
					buf_append(ab, c+i, 1);
				}
			}
			buf_append(ab, "\x1b[39m", 5);
		}
		buf_append(ab, "\x1b[K", 3);
      	buf_append(ab, "\r\n", 2);
	}
}

void editor_draw_status_bar (struct abuf *ab) { // Different from Orig
	char status[editor.window_cols], rstatus[80];
	snprintf(rstatus, sizeof(rstatus), "%s | %d/%d", 
							editor.syntax ? editor.syntax->filetype : "no ft",
							editor.cy+1, editor.num_rows);
	snprintf(status, sizeof(status)+1, "%-20.20s%-10s%*s", editor.filename ? editor.filename : "[NO-NAME]",
					editor.dirty ? "(modified)" : "", 
					editor.window_cols-30, rstatus);
	buf_append(ab, "\x1b[7m", 4);
	buf_append(ab, status, editor.window_cols);
	buf_append(ab, "\x1b[m", 3);
	buf_append(ab, "\r\n", 2); // For status message
}

void editor_draw_message_bar (struct abuf *ab) {
	buf_append(ab, "\x1b[K", 3);
	int msg_len = strlen(editor.statusmsg);
	if (msg_len > editor.window_cols) msg_len = editor.window_cols;
	if (msg_len && (time(NULL) - editor.statusmsg_time < 5))
		buf_append(ab, editor.statusmsg, msg_len);
}

void editor_refresh_screen() {
	struct abuf ab = ABUF_INIT;
	editor_scroll();
	buf_append(&ab, "\x1b[?25l", 6);
	buf_append(&ab, "\x1b[H", 3);
	editor_draw_rows(&ab);
	editor_draw_status_bar(&ab);
	editor_draw_message_bar(&ab);
	char buf[32];
	// Adding 1 to cursor pos bcoz terminal uses 1-based index
	snprintf(buf, sizeof buf, "\x1b[%d;%dH", (editor.cy - editor.row_offset)+1, (editor.rx - editor.col_offset)+1);
	buf_append(&ab, buf, strlen(buf));
	buf_append(&ab, "\x1b[?25h", 6);
	write(STDOUT_FILENO, ab.buf, ab.len);
	buf_free(&ab);
}

void editor_set_status_message(const char *fmt, ...) {
	va_list ap;
	va_start(ap, fmt);
	vsnprintf(editor.statusmsg, sizeof(editor.statusmsg), fmt, ap);
	va_end(ap);
	editor.statusmsg_time = time(NULL);
}

/*** init ***/

void init_editor() {
	editor.cx = 0;
	editor.cy = 0;
	editor.rx = 0;
	editor.num_rows = 0;
	editor.dirty = 0;
	editor.e_row = NULL;
	editor.row_offset = 0;
	editor.col_offset = 0;
	if (get_window_size(&editor.window_rows, &editor.window_cols) == -1) pkill("get_window_size");
	editor.window_rows -= 2; // For status bar and message at the bottom
	editor.filename = NULL;
	editor.statusmsg[0] = '\0';
	editor.statusmsg_time = 0;
	editor.syntax = NULL;
}

int main(int argc, char* argv[]) {
	atexit(disable_raw_mode); // Different from Orig
	enable_raw_mode();
	init_editor();
	if (argc >= 2)
		editor_open(argv[1]);
	editor_set_status_message("HELP: Ctrl-S = save | Ctrl-Q = quit | Ctrl-F = find | Ctrl-G = seek");
	while(1) {
		editor_refresh_screen();
		editor_process_keypress();
	};
	return 0;
}
